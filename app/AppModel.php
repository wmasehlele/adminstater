<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\User;
use App\Role;
use App\Permission;

class AppModel extends Model
{
    public static function setupLoggedinUserCompany() {
        $user = Auth::user();
        $userFound = User::find($user->id); // we need to fix this
        if (!empty($userFound)) {
            return Company::find($userFound->company_id);    
        }
        return [];
    }

    public static function setupLoggedinUserPermissions () {
        $user = Auth::user();
        $userFound = User::find($user->id);
        if (!empty($userFound->user_roles)) {
            $role = Role::find($userFound->user_roles->role_id); // role and role permissions found...
            $permisions_ids = [];
            foreach($role->role_permissions as $key=>$role_permission) {
                array_push($permisions_ids, $role_permission->permission_id);
            }
            $permissions = Permission::whereIn('id',$permisions_ids)->get();
            return [
                'user_roles' => $role,
                'user_permissions' => $permissions
            ];  
        }
        return [];
    }

    public static function lists ()  {
        return [
            'genders' => [
                'none' => 'None',
                'female' => 'Female',
                'male' => 'Male',
                'other' => 'Other'
            ],
            'marital_statuses' => [
                'none' => 'None',
                'devorced' => 'Divorced',
                'married' => 'Married',
                'single' => 'Single',
                'other' => 'Other'
            ],
            'provinces' => [
                'none' => 'None',
                'gautent' => 'Gauteng',
                'limpopo' => 'Limpopo',
                'mpumalanga' => 'Mpumalanga',
                'kwa_zulu_natal' => 'Kwa-Zulu Natal',
                'free_state' => 'Free State',
                'northern_cape' => 'Northern Cape',
                'north_west' => 'North West',
                'eastern_cape' => 'Eastern Cape',
                'western_cape' => 'Western Cape'
            ],
            'contact_relationships' => [
                'sister' => 'Sister',
                'brother' => 'Brother',
                'mother' => 'Mother',
                'father' => 'Father',
                'spouse' => 'Spouse',
                'child' => 'Child',
                'friend' => 'Friend'
            ],
            'cover_types' => [
                'main_member' => 'Main Member',
                'beneficiary' => 'Beneficiary'
            ],
            'contact_types' => [
                'cellphone' => 'Cellphone',
                'email' => 'Email',
                'home' => 'Home',
                'work' => 'Work Tel'
            ],
            'address_types' => [
                'home' => 'Home',
                'work' => 'Work',
                'postal' => 'Postal'
            ],            
            'contact_priority_types' => [
                'primary' => 'Primary',
                'alternative' => 'Alternative'
            ]
        ];                        
    }
}
