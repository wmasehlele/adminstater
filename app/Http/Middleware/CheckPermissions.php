<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Session;
use App\AppModel;


class CheckPermissions
{
    /**
     * Handle an incoming request.
     *  Check if the logged in user has permission to a module. 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected $session;

    public function handle($request, Closure $next)
    {        
        $route = $request->path();            
        if (strrpos($route, '/') > 0) {
            $route = explode('/', $route)[0]; 
        }        
        if ( \App\User::userHasPermission($route) ){     
            return $next($request);
        }        
        \Session::forget('url.intended');
        return abort(403);
    }
}
    