<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AppModel;
use App\Company;
use App\User;

class AddressController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_address(Request $request) {
        if (User::userHasRights('clients', 'can_write')) {
            if (!empty($request->input('id'))){
                $address = \App\Address::find($request->input('id'));
            } else {
                $address = new \App\Address();
            }
            $address->type = $request->input('type');
            $address->address_line_1 = $request->input('address_line_1');
            $address->address_line_2 = $request->input('address_line_2');
            $address->address_line_3 = $request->input('address_line_3');
            $address->address_line_4 = $request->input('address_line_4');
            $address->client_id = $request->input('client_id');
            try{ 
                $address->save();
                return json_encode(['status' => true, 'message' => 'Address details saved.']);
            } catch (\Exception $ex) {
                Log::debug('Unable to create new contact: '. $ex->getMessage());
                return json_encode(['status' => false, 'message' => 'Failed to save address details, please try again.']);
            }
        }
    }

    public function client_addresses(Request $request, $client_id) {
        $adresses = \App\Address::where('client_id', $client_id)->get();
        return $adresses;
    }

    public function client_address(Request $request, $client_id, $address_id) {
        $address = \App\Address::where(['id' => $address_id, 'client_id' => $client_id])->first();
        return $address;
    }

    public function delete_address(Request $request) {
        $address = \App\Address::where(['id' => $request->input('id'), 'client_id' => $request->input('client_id')])->first();
        if (!empty($address)) {
            try{
                $address->delete();
                return json_encode(['status' => true, 'message' => 'Address deleted.']);
            } catch (\Exception $ex) {
                Log::debug($ex->getMessage());
                return json_encode(['status' => false, 'message' => 'Failed to delete address details, please try again.']);
            }
        } else {
            return json_encode(['status' => false, 'message' => 'Failed to delete address details, client must have atleast one address.']);
        }
    }
}
