<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\AppModel;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected function redirectTo() {
        $this->loggedin_user = AppModel::setupLoggedinUserPermissions();
        if (!empty($this->loggedin_user['user_permissions'])){
            return '/'.$this->loggedin_user['user_permissions'][0]['technical_name'];
        } else {
            return '/home'; 
        }
    }

    protected function validateLogin(Request $request) {
        $this->validate($request, [
                $this->username() => 'exists:users,' . $this->username() . ',active,1',
                'password' => 'required|string',
            ]
        );
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
