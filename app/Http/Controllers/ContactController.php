<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AppModel;
use App\Company;
use App\User;

class ContactController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_contact(Request $request) {
        if (User::userHasRights('clients', 'can_write')) {
            if (!empty($request->input('id'))){
                $contact = \App\Contact::find($request->input('id'));
                if (in_array($contact->type, ['cellphone', 'email'])) {
                    $type = $request->input('type');
                    $detail = $request->input('detail');
                    $client = \App\Client::where(['id' => $contact->client_id, $contact->type => $contact->detail])->first();                    
                    if (!empty($client)) {
                        $client->$type = $request->input('detail');
                        $client->save();
                    }
                }                

            } else {
                $contact = new \App\Contact();
            }
            $contact->type = $request->input('type');
            $contact->detail = $request->input('detail');
            $contact->priority = $request->input('priority');
            $contact->client_id = $request->input('client_id');
            try{ 
                $contact->save();
                return json_encode(['status' => true, 'message' => 'Contact details saved.']);
            } catch (\Exception $ex) {
                Log::debug('Unable to save contact details: '. $ex->getMessage());
                return json_encode(['status' => false, 'message' => 'Failed to save contact details, please try again.']);
            }
        }
    }

    public function client_contacts(Request $request, $client_id) {
        $contacts = \App\Contact::where('client_id', $client_id)->get();
        return $contacts;
    }

    public function client_contact(Request $request, $client_id, $contact_id) {
        $contact = \App\Contact::where(['id' => $contact_id, 'client_id' => $client_id])->first();
        return $contact;
    }

    public function delete_contact(Request $request) {
        $contact = \App\Contact::where(['id' => $request->input('id'), 'client_id' => $request->input('client_id')])->first();
        if (!empty($contact)) {
            try{
                $contact->delete();
                return json_encode(['status' => true, 'message' => 'Contact deleted.']);
            } catch (\Exception $ex) {
                Log::debug($ex->getMessage());
                return json_encode(['status' => false, 'message' => 'Failed to delete contact details, please try again.']);
            }
        } else {
            return json_encode(['status' => false, 'message' => 'Failed to delete contact details, client must have atleast one contact.']);
        }
    }
}
