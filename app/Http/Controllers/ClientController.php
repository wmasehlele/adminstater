<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AppModel;
use App\Company;
use App\User;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user_company = [];
    protected $accepted_roles = ['super_administrator', 'company_administrator', 'branch_administrator'];

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        User::userHasRole($this->accepted_roles);
        
        $this->loggedin_user = AppModel::setupLoggedinUserPermissions();
        $this->user_company = AppModel::setupLoggedinUserCompany();
        if (!empty($this->loggedin_user) && !empty($this->user_company)){
            return view('client', [
                    'company' => $this->user_company->company,
                    'branches' => $this->user_company->branches,
                    'company_setups' => $this->user_company->company_setups,
                    'user_roles' => $this->loggedin_user['user_roles'],
                    'user_permissions' => $this->loggedin_user['user_permissions']
                ]
            );
        }
        return "Unauthorised User";
    }

    public function view_client (Request $request, $id) {
        $this->user_company = AppModel::setupLoggedinUserCompany();        
        $cliet = \App\Client::where(['id' => $id, 'active' => true, 'company_id' => $this->user_company->id])->first();
        return $cliet;
    }

    public function company_clients (Request $request) {
        $action = $route = $request->path(); 
        //dd(explode('/', $action));

        $this->user_company = AppModel::setupLoggedinUserCompany();   
        $cliets = \App\Client::where(['company_id' => $this->user_company->id, 'active' => true])->paginate(3);            
        return $cliets;
    }

    public function update_client (Request $request) {
        if (User::userHasRights('clients', 'can_execute')) {
            if (!empty($request->input('id'))) {
                $client = \App\Client::where(['id' => $request->input('id'), 'active' => true])->first();
                $client->name = $request->input('name');
                $client->surname = $request->input('surname');
                $client->id_number = $request->input('id_number');
                $client->passport_number = $request->input('passport_number');
                $client->birth_date = date_format(date_create($request->input('birth_date')), 'Y-m-d');
                $client->gender = $request->input('gender');
                $client->marital_status = $request->input('marital_status');
                $client->nationality = $request->input('nationality');
                $client->active = true;
                $client->company_id = $request->input('company_id');
                try {
                    $client->save();
                    return json_encode(['status' => true, 'message' => 'Client updated.', 'client' => $client ]);
                } catch (\Exception $ex) {
                    Log::debug('Unable to update client: '. $ex->getTraceAsString());
                    return json_encode(['status' => false, 'message' => 'Failed to update client, please try again.']);
                }
            }
        }
    }

    public function create_client (Request $request) {
        if (User::userHasRights('clients', 'can_write')) {
            $this->user_company = AppModel::setupLoggedinUserCompany();
            $client = new \App\Client();
            $client->name = $request->input('name');
            $client->surname = $request->input('surname');
            $client->id_number = $request->input('id_number');
            $client->email = $request->input('email');
            $client->cellphone = $request->input('cellphone');
            $client->passport_number = $request->input('passport_number');
            $client->birth_date = date_format(date_create($request->input('birth_date')), 'Y-m-d');
            $client->gender = $request->input('gender');
            $client->marital_status = $request->input('marital_status');
            $client->nationality = $request->input('nationality');
            $client->active = true;
            $client->company_id = $this->user_company->id;

            try{
                // duplicate id number or passport number is not allowed....
                if (!empty($request->input('id_number'))) {
                    $clientExist = \App\Client::where('id_number', $request->input('id_number'))->first();
                    if (!empty($clientExist)) {
                        return json_encode(['status' => false, 'message' => 'The id number is already in use.']);
                    }
                }

                if (!empty($request->input('passport_number'))) {
                    $clientExist = \App\Client::where('passport_number', $request->input('passport_number'))->first();
                    if (!empty($clientExist)) {
                        return json_encode(['status' => false, 'message' => 'The passport number is already in use.']);
                    }
                }

                DB::beginTransaction();
                // save new client
                $client->save();
                if (!empty($request->input('email'))) {
                    $contact = new \App\Contact();
                    $contact->type = 'email';
                    $contact->detail = $request->input('email');
                    $contact->priority = 'primary';
                    $contact->client_id = $client->id;
                    $contact->save();
                }
                if (!empty($request->input('cellphone'))) {
                    $contact = new \App\Contact();
                    $contact->type = 'cellphone';
                    $contact->detail = $request->input('cellphone');
                    $contact->priority = 'primary';
                    $contact->client_id = $client->id;
                    $contact->save();
                }
                if (!empty($request->input('address_line_1'))) {
                    // save the addres...
                    $address = new \App\Address();
                    $address->address_line_1 = $request->input('address_line_1');
                    $address->address_line_2 = $request->input('address_line_2');
                    $address->address_line_3 = $request->input('address_line_3');
                    $address->address_line_4 = $request->input('address_line_4');
                    $address->type = 'home';
                    $address->client_id = $client->id;
                    $address->save();
                }
                $userExist = User::where('email', $request->input('email'))->first();
                if (!empty($userExist)) {
                    return json_encode(['status' => false, 'message' => 'The email address is already in use.']);
                }
                // create new user
                $user = new User();
                $user->name = $request->input('name');
                $user->surname = $request->input('surname');
                $user->email = $request->input('email');
                $user->cellphone = $request->input('cellphone');
                $user->type = 'customer';
                $user->model = 'Client';
                $user->model_id = $client->id;
                $user->company_id = 0;// $this->user_company->id;
                $user->password = Hash::make('afudjk7f676ewre48897sfhb');
                $user->save();
                // update client's user_id
                $update_client = \App\Client::find($client->id);
                $update_client->user_id = $user->id;
                $update_client->save();
                DB::commit();
                return json_encode(['status' => true, 'message' => 'New client created.']);
            } catch (\Exception $ex) {
                DB::rollBack();
                Log::debug('Unable to create new client: '. $ex->getTraceAsString());
                return json_encode(['status' => false, 'message' => 'Failed to create new client, please try again.']);
            }
        } else {
            return json_encode(['status' => false, 'message' => 'Unauthorised, you do not have the right permissions.']);
        }
    }

    public function delete_client (Request $request) {
        $client = \App\Client::where(['id' => $request->input('id'), 'company_id' => $request->input('company_id')])->first();
        if (!empty($client)) {
            try{
                $user = \App\User::find($client->user_id);
                $user->delete();    
                $client->name = "XXXXXXXXXX";
                $client->surname = "XXXXXXXXXX";
                $client->id_number = "XXXXXXXXXX";
                $client->email = "XXXXXXXXXX";
                $client->cellphone = "XXXXXXXXXX";
                $client->passport_number = "XXXXXXXXXX";
                $client->car_registration = "XXXXXXXXXX";
                $client->company_id = 00000;
                $client->user_id = 00000;
                $client->active = false;
                $client->save();
                return json_encode(['status' => true, 'message' => 'Client deleted.']);
            } catch (\Exception $ex) {
                Log::debug($ex->getMessage());
                return json_encode(['status' => false, 'message' => 'Failed to delete client, please try again.']);
            }
        } else {
            return json_encode(['status' => false, 'message' => 'Failed to delete client, client not found.']);
        }
    }
}
