<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\AppModel;
use App\Company;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $user_company = [];
    protected $accepted_roles = ['super_administrator', 'administrator', 'customer'];

    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('hasPermission');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        User::userHasRole($this->accepted_roles);

        $this->loggedin_user = AppModel::setupLoggedinUserPermissions();
        $this->user_company = AppModel::setupLoggedinUserCompany();
        if (!empty($this->loggedin_user) && !empty($this->user_company)){
            return view('user', [
                    'company' => $this->user_company->company,
                    'branches' => $this->user_company->branches,
                    'company_setups' => $this->user_company->company_setups,
                    'user_roles' => $this->loggedin_user['user_roles'],
                    'user_permissions' => $this->loggedin_user['user_permissions']
                ]
            );
        }
        return "Unauthorised User";
    }

    public function company_users (Request $request) {
        $this->user_company = AppModel::setupLoggedinUserCompany();   
        $users = \App\User::where(['type' => 'administrator', 'company_id' => $this->user_company->id])->paginate(3);            
        return $users;
    }
}
