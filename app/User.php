<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use App\Company;
use App\User;
use App\Role;
use App\Permission;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function companies() {
        return $this->hasOne('App\Company');
    }   
    
    public function branch_users() {
        return $this->hasOne('App\BranchUser');
    }  

    public function user_roles() {
        return $this->hasOne('App\UserRole');
    }  

    public static function user_types () {
        return [
            'customer' => 'Customer',
            'company' => 'Company Administrator',
            'branch' => 'Branch Administrator',
            'super_admin' => 'Super Administrator'
        ];
    }
    // send here an array of acceptable roles for the controller/module. 
    public static function userHasRole ($acceptable_roles) { // checks if user has been assigned one of the admin roles
        $user = Auth::user();
        $userFound = User::find($user->id);
        if (!empty($userFound->user_roles)) {            
            $role = Role::find($userFound->user_roles->role_id); 
            if (!in_array($role->technical_name, $acceptable_roles)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static function userHasRights ($action_right) { // checks if user has rights to write or execute in the module
        $user = Auth::user();
        $userFound = User::find($user->id);
        if (!empty($userFound->user_roles)) {            
            $role = Role::find($userFound->user_roles->role_id);
            $permisions_ids = [];
            foreach($role->role_permissions as $key=>$role_permission) {
                if ($role_permission->$action_right == true) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function userHasPermission ($action_permission) { // checks if users has access to module.
        $user = Auth::user();
        $userFound = User::find($user->id);
        if (!empty($userFound->user_roles)) {
            $permission = Permission::where(['technical_name' => $action_permission])->first();
            if (!empty($permission)) {
                $role_permission = RolePermission::where(['role_id' => $userFound->user_roles->role_id, 'permission_id' => $permission->id])->first();
                if (!empty($role_permission)) {
                    return true;
                }
            }
        }        
        return false;
    }
}
