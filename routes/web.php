<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\CheckPermissions;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('website.welcome');
});

Auth::routes();

Route::get('/lists', function () {
    return json_encode(\App\AppModel::lists());
})->name('lists');

Route::middleware([CheckPermissions::class])->group(function () {        
    // Home module routes
    Route::get('/home', 'HomeController@index')->name('home');
    
    // Calendar module routes
    Route::get('/calendar', 'BookingController@index')->name('calendar');
    
    // Users module routes
    Route::get('/users', 'UserController@index')->name('users');
    Route::get('/users/company_users', 'UserController@company_users')->name('users.company_users');

    // Reports module routes
    Route::get('/reports', 'ReportController@index')->name('reports');    
    
    // Branch settings module routes
    Route::get('/branch_settings', 'ReportController@index')->name('branch_settings');

    // Company settings module routes
    Route::get('/company_settings', 'ReportController@index')->name('company_settings');

    // Clients module routes
    Route::get('/clients', 'ClientController@index')->name('clients');
    Route::get('/clients/company_clients', 'ClientController@company_clients')->name('clients.company_clients');
    Route::get('/clients/view_client/{id}', 'ClientController@view_client')->name('clients.view_client');
    Route::post('/clients/create_client', 'ClientController@create_client')->name('clients.create_client');
    Route::post('/clients/update_client', 'ClientController@update_client')->name('clients.update_client');
    Route::post('/clients/delete_client', 'ClientController@delete_client')->name('clients.delete_client');

    Route::get('/clients/contacts/client_contacts/{client_id}', 'ContactController@client_contacts')->name('clients.client_contacts');
    Route::get('/clients/contacts/client_contact/{client_id}/{contact_id}', 'ContactController@client_contact')->name('clients.client_contact');
    Route::post('/clients/contacts/create_contact', 'ContactController@create_contact')->name('clients.create_contact');
    Route::post('/clients/contacts/delete_contact', 'ContactController@delete_contact')->name('clients.delete_contact');    

    Route::get('/clients/addresses/client_addresses/{client_id}', 'AddressController@client_addresses')->name('clients.get_client_addresses');
    Route::get('/clients/addresses/client_address/{client_id}/{address_id}', 'AddressController@client_address')->name('clients.get_client_address');
    Route::post('/clients/addresses/create_address', 'AddressController@create_address')->name('clients.create_address');
    Route::post('/clients/addresses/delete_address', 'AddressController@delete_address')->name('clients.delete_address');
});