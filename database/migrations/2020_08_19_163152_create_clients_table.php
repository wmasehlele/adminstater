<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->char('name', 50);
            $table->char('surname', 50);
            $table->char('id_number', 15)->nullable()->unique();
            $table->char('passport_number', 15)->nullable()->unique();
            $table->date('birth_date', 15)->nullable();
            $table->char('gender', 10)->nullable();
            $table->char('marital_status', 10)->nullable();
            $table->char('nationality', 50)->nullable();
            $table->char('cellphone', 15)->nullable();
            $table->char('email', 50)->nullable();
            $table->char('car_registration', 15)->nullable();
            $table->char('car_make', 15)->nullable();
            $table->char('car_model', 15)->nullable();
            $table->char('car_colour', 15)->nullable();
            $table->boolean('active', true);
            $table->integer('user_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
