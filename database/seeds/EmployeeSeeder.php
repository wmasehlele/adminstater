<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->truncate();
        DB::table('employees')->insert([
            'active' => true,
            'user_id' => 3,
        ]);
    }
}
