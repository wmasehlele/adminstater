<?php

use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 5,
            'can_write' => 1,
            'can_execute' => 1
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 6,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 7,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 2,
            'permission_id' => 8,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 3,
            'permission_id' => 1,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 3,
            'permission_id' => 2,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 3,
            'permission_id' => 3,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
        DB::table('role_permissions')->insert([
            'role_id' => 3,
            'permission_id' => 4,
            'can_write' => 1,
            'can_execute' => 1            
        ]);
    }
}
