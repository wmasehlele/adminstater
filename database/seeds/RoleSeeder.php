<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'Super Administrator',
            'technical_name' => 'super_administrator',
        ]);
        DB::table('roles')->insert([
            'name' => 'Company Administrator',
            'technical_name' => 'company_administrator',
        ]);
        DB::table('roles')->insert([
            'name' => 'Branch Administrator',
            'technical_name' => 'branch_administrator',
        ]);
        DB::table('roles')->insert([
            'name' => 'Reception',
            'technical_name' => 'reception',
        ]);
        DB::table('roles')->insert([
            'name' => 'Doctor',
            'technical_name' => 'doctor',
        ]);
        DB::table('roles')->insert([
            'name' => 'Nurse',
            'technical_name' => 'nurse',
        ]);
        DB::table('roles')->insert([
            'name' => 'Staff',
            'technical_name' => 'staff',
        ]);
    }
}
