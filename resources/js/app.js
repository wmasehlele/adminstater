/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

var module = window.location.href.split('/')[3].replace('#','');

import Vue from 'vue';
import VueRouter from "vue-router";
import loader from "vue-ui-preloader";
import PopUpDialog from 'vuejs-dialog';
import Datepicker from 'vuejs-datepicker';

import 'vuejs-dialog/dist/vuejs-dialog.min.css';

import homeRouter from "./modules/home"; 
import clientsRouter from "./modules/clients"; 
import calendarRouter from "./modules/calendar"; 
import reportsRouter from "./modules/reports"; 
import branchSettingsRouter from "./modules/branch_settings"; 
import companySettingsRouter from "./modules/company_settings"; 
import usersRouter from "./modules/users"; 

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component("Datepicker", Datepicker);
 
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter); 
Vue.use(loader);
Vue.use(PopUpDialog);
Vue.use(Datepicker);

Vue.mixin({
    data: function() {
        return {
            lists: {},
            _client_id: 0
        }
    },
    created: function () {

    },
    methods: {
        isValidEmail: function (email) {
            alert('Email is valid');
        },
        getLists: function () {
            var _this = this;
            axios.get('/lists').then(function (response) {
                _this.lists = response.data;
            }).catch(function (error) { }); 
        }
    }    
});

var appConfig = {
    el: '#app',
    data: {},
    beforeCreate () {},    
    created () {},
    computed: {},
    methods: {}
};

switch (module) {
    case 'home': 
        appConfig.router = homeRouter;
        break;        
    case 'clients': 
        appConfig.router = clientsRouter;
        break;
    case 'calendar': 
        appConfig.router = calendarRouter;
        break;    
    case 'reports': 
        appConfig.router = reportsRouter;
        break;   
    case 'branch_settings': 
        appConfig.router = branchSettingsRouter;
        break;
    case 'company_settings': 
        appConfig.router = companySettingsRouter;
        break;                                 
    case 'users':
        appConfig.router = usersRouter;
        break;
    default:
        appConfig.router = homeRouter;
        break;             
}

const app = new Vue(appConfig);

Vue.config.productionTip = false;