import Router from "vue-router";

import UsersComponent from '../components/users/UsersComponent.vue';
import ViewUserComponent from '../components/users/ViewUserComponent.vue';
import UserDetailsComponent from '../components/users/UserDetailsComponent.vue';
import RolesPermissionsComponent from '../components/users/RolesPermissionsComponent.vue';
import CreateUserComponent from '../components/users/CreateUserComponent.vue';
// import EmergencyContactComponent from '../components/clients/EmergencyContactComponent.vue';
// import MedicalAidDetailsComponent from '../components/clients/MedicalAidDetailsComponent.vue';

export default new Router({
    routes: [
        { path: '/', name:'users', props: true, component: UsersComponent },
        { path: '/view/:user_id', props: true, component: ViewUserComponent,
            children: [
                { path: '/', name: 'view-user', props: true, component: UserDetailsComponent },
                { path: '/view/:user_id/info', name: 'user-details', props: true, component: UserDetailsComponent },
                { path: '/view/:user_id/roles', name: 'roles-permissions', props: true, component: RolesPermissionsComponent }
            ]
        },   
        { path: '/create', props: true, component: CreateUserComponent }             
    ]
});