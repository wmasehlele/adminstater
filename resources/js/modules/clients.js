import Vue from 'vue';
import Router from "vue-router";

import ClientsComponent from '../components/clients/ClientsComponent.vue';
import ViewClientComponent from '../components/clients/ViewClientComponent.vue';
import ClientInformationComponent from '../components/clients/ClientInformationComponent.vue';
import ContactInformationComponent from '../components/clients/ContactInformationComponent.vue';
import CreateClientComponent from '../components/clients/CreateClientComponent.vue';
import CreateContactComponent from '../components/clients/CreateContactComponent.vue';
import CreateAddressComponent from '../components/clients/CreateAddressComponent.vue';

export default new Router({
    routes: [
        { path: '/', name: 'clients', props: true, component: ClientsComponent },
        { path: '/view/:client_id', props: true, component: ViewClientComponent,
            children: [
                { path: '/', name: 'view-client', props: true, component: ClientInformationComponent },
                { path: '/view/:client_id/info', name: 'client-info', props: true, component: ClientInformationComponent },
                
                { path: '/view/:client_id/contacts', name: 'contact-info', props: true, component: ContactInformationComponent },

                { path: '/view/:client_id/create_contact', name: 'create-contact', props: true, component: CreateContactComponent },
                { path: '/view/:client_id/create_contact/:contact_id', name: 'edit-contact', props: true, component: CreateContactComponent },

                { path: '/view/:client_id/create_address', name: 'create-address', props: true, component: CreateAddressComponent },
                { path: '/view/:client_id/create_address/:address_id', name: 'edit-address', props: true, component: CreateAddressComponent }
            ]
        }, 
        { path: '/create', props: true, component: CreateClientComponent }
    ]
});