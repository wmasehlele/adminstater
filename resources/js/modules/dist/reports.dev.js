"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _ClientsComponent = _interopRequireDefault(require("../components/clients/ClientsComponent.vue"));

var _CrmComponent = _interopRequireDefault(require("../components/clients/CrmComponent.vue"));

var _ClientInformationComponent = _interopRequireDefault(require("../components/clients/ClientInformationComponent.vue"));

var _MedicalAidComponent = _interopRequireDefault(require("../components/clients/MedicalAidComponent.vue"));

var _CreateClientComponent = _interopRequireDefault(require("../components/clients/create-client/CreateClientComponent.vue"));

var _AllClientsComponent = _interopRequireDefault(require("../components/clients/AllClientsComponent.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = new _vueRouter["default"]({
  routes: [{
    path: '/',
    component: _ClientsComponent["default"],
    children: [{
      path: '/',
      name: 'all',
      props: true,
      component: _AllClientsComponent["default"]
    }, {
      path: '/create',
      name: 'create',
      props: true,
      component: _CreateClientComponent["default"]
    }]
  }, {
    path: '/view/:client_id',
    component: _CrmComponent["default"],
    children: [{
      path: '/view/:client_id/',
      name: 'client',
      props: true,
      component: _ClientInformationComponent["default"]
    }, {
      path: '/view/:client_id/info',
      name: 'clientInfo',
      props: true,
      component: _ClientInformationComponent["default"]
    }, {
      path: '/view/:client_id/medical_aid',
      name: 'medicalAid',
      props: true,
      component: _MedicalAidComponent["default"]
    }]
  }]
});

exports["default"] = _default;