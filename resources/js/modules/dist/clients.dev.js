"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _ClientsComponent = _interopRequireDefault(require("../components/clients/ClientsComponent.vue"));

var _CrmComponent = _interopRequireDefault(require("../components/clients/CrmComponent.vue"));

var _ClientInformationComponent = _interopRequireDefault(require("../components/clients/ClientInformationComponent.vue"));

var _MedicalAidComponent = _interopRequireDefault(require("../components/clients/MedicalAidComponent.vue"));

var _AllClientsComponent = _interopRequireDefault(require("../components/clients/AllClientsComponent.vue"));

var _CreateClientComponent = _interopRequireDefault(require("../components/clients/create-client/CreateClientComponent.vue"));

var _PersonalDetailsComponent = _interopRequireDefault(require("../components/clients/create-client/PersonalDetailsComponent.vue"));

var _ContactDetailsComponent = _interopRequireDefault(require("../components/clients/create-client/ContactDetailsComponent.vue"));

var _PhysicalAddressComponent = _interopRequireDefault(require("../components/clients/create-client/PhysicalAddressComponent.vue"));

var _EmergencyContactComponent = _interopRequireDefault(require("../components/clients/create-client/EmergencyContactComponent.vue"));

var _MedicalAidDetailsComponent = _interopRequireDefault(require("../components/clients/create-client/MedicalAidDetailsComponent.vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = new _vueRouter["default"]({
  routes: [{
    path: '/',
    component: _ClientsComponent["default"],
    children: [{
      path: '/',
      name: 'all',
      props: true,
      component: _AllClientsComponent["default"]
    }, {
      path: '/create',
      name: 'create',
      props: true,
      component: _CreateClientComponent["default"],
      children: [{
        path: '/',
        name: 'peronal-info',
        props: true,
        component: _PersonalDetailsComponent["default"]
      }, {
        path: '/create/contact-info',
        name: 'contact-info',
        props: true,
        component: _ContactDetailsComponent["default"]
      }, {
        path: '/create/address-info',
        name: 'address-info',
        props: true,
        component: _PhysicalAddressComponent["default"]
      }, {
        path: '/create/mergency-contact',
        name: 'mergency-contact',
        props: true,
        component: _EmergencyContactComponent["default"]
      }, {
        path: '/create/medical-aid',
        name: 'medical-aid',
        props: true,
        component: _MedicalAidDetailsComponent["default"]
      }]
    }]
  }, {
    path: '/view/:client_id',
    component: _CrmComponent["default"],
    children: [{
      path: '/view/:client_id/',
      name: 'client',
      props: true,
      component: _ClientInformationComponent["default"]
    }, {
      path: '/view/:client_id/info',
      name: 'clientInfo',
      props: true,
      component: _ClientInformationComponent["default"]
    }, {
      path: '/view/:client_id/medical_aid',
      name: 'medicalAid',
      props: true,
      component: _MedicalAidComponent["default"]
    }]
  }]
});

exports["default"] = _default;