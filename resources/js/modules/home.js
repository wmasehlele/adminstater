import Router from "vue-router";

import ClientsComponent from '../components/clients/ClientsComponent.vue';
import ViewClientComponent from '../components/clients/ViewClientComponent.vue';
import ClientInformationComponent from '../components/clients/ClientInformationComponent.vue';
import ContactInformationComponent from '../components/clients/ContactInformationComponent.vue';
import CreateClientComponent from '../components/clients/CreateClientComponent.vue';
// import EmergencyContactComponent from '../components/clients/EmergencyContactComponent.vue';
// import MedicalAidDetailsComponent from '../components/clients/MedicalAidDetailsComponent.vue';

//Vue.component('contact-details-component', { props: ['customer_id'], ContactDetailsComponent });
export default new Router({
    routes: [
        { path: '/', name:'clients', props: true, component: ClientsComponent },
        { path: '/view/:client_id', props: true, component: ViewClientComponent,
            children: [
                { path: '/', name: 'view-client', props: true, component: ClientInformationComponent },
                { path: '/view/:client_id/info', name: 'client-info', props: true, component: ClientInformationComponent },
                { path: '/view/:client_id/contacts', name: 'contact-info', props: true, component: ContactInformationComponent }
            ]
        },                
    ]
});