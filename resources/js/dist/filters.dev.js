"use strict";

var _vue = _interopRequireDefault(require("vue"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_vue["default"].filter("capitalize", function (value) {
  if (!value) {
    return "";
  }

  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});

_vue["default"].filter("cutText", function (value, length, suffix) {
  if (value.length > length) {
    return value.substring(0, length) + suffix;
  } else {
    return value;
  }
});