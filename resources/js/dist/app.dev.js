"use strict";

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

var _vueUiPreloader = _interopRequireDefault(require("vue-ui-preloader"));

var _vuejsDialog = _interopRequireDefault(require("vuejs-dialog"));

var _vuejsDatepicker = _interopRequireDefault(require("vuejs-datepicker"));

require("vuejs-dialog/dist/vuejs-dialog.min.css");

var _home = _interopRequireDefault(require("./modules/home"));

var _clients = _interopRequireDefault(require("./modules/clients"));

var _calendar = _interopRequireDefault(require("./modules/calendar"));

var _reports = _interopRequireDefault(require("./modules/reports"));

var _branch_settings = _interopRequireDefault(require("./modules/branch_settings"));

var _company_settings = _interopRequireDefault(require("./modules/company_settings"));

var _users = _interopRequireDefault(require("./modules/users"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');

var _module = window.location.href.split('/')[3].replace('#', '');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
_vue["default"].component("Datepicker", _vuejsDatepicker["default"]); // const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


_vue["default"].use(_vueRouter["default"]);

_vue["default"].use(_vueUiPreloader["default"]);

_vue["default"].use(_vuejsDialog["default"]);

_vue["default"].use(_vuejsDatepicker["default"]);

var appConfig = {
  el: '#app'
};

switch (_module) {
  case 'home':
    appConfig.router = _home["default"];
    break;

  case 'clients':
    appConfig.router = _clients["default"];
    break;

  case 'calendar':
    appConfig.router = _calendar["default"];
    break;

  case 'reports':
    appConfig.router = _reports["default"];
    break;

  case 'branch_settings':
    appConfig.router = _branch_settings["default"];
    break;

  case 'company_settings':
    appConfig.router = _company_settings["default"];
    break;

  case 'users':
    appConfig.router = _users["default"];
    break;

  default:
    appConfig.router = _home["default"];
    break;
}

appConfig.methods = {
  openPicker: function openPicker() {
    this.$refs.programaticOpen.showCalendar();
  }
};
var app = new _vue["default"](appConfig);
_vue["default"].config.productionTip = false;