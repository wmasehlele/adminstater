"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;
var _default = {
  data: function data() {
    return {
      msg: 'Hello World'
    };
  },
  created: function created() {//console.log('Printing from the Mixin');
  },
  methods: {
    displayMessage: function displayMessage() {
      console.log('Now printing from a mixin function');
    }
  }
};
exports["default"] = _default;