@extends('layouts.app', ['company' => $company, 'branches' => $branches, 'company_setups'=>$company_setups, 'user_roles' => $user_roles, 'user_permissions' => $user_permissions])

@section('content')
<div class="container-fluid"> 
    <div class="card content-wrapper">
        <div class="card-header module-header text-uppercase h5">
                @foreach($user_permissions as $user_permission)         
                    <?=Route::currentRouteName() == $user_permission->technical_name ? __($user_permission->name) : '' ?>
                @endforeach        
        </div>
        <div class="card-body"> 
            <div class="content-view">
                <router-view></router-view>
            <div>
        </div>
    </div>
</div>
@endsection
